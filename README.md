# Bolger

Audio visualizer

## Building

```console
$ nix-shell
$ cargo build --release # or: cargo run --realease
```

## Installing

To install for NixOS:
Add the following line to your configuration.nix in environment.systemPackages
```nix
(pkgs.callPackage path-to-default.nix {})
```

Then copy the configuration file and shaders to your config directory
```console
$ mkdir -p $HOME/.config/{bolger,bolger/shaders}
$ cp bolger.toml $HOME/.config/bolger/bolger.toml
$ cp -r ./shaders/ $HOME/.config/bolger/shaders
```

Then edit the following two lines `$HOME/.config/bolger/bolger.toml`
```toml
vert_shader = "" # Place the path you copied the vertex shader to in the last step
frag_shader = ""   # Place the path you copied the vertex shader to in the last step 
```

## Customization

Bolger is cutomized using a TOML file

```toml
output = "opengl"

[sdl]
bg_image = "test.png"                 # Background image to display
bg_image_color_mod = "#82EEFD"        # Color mod for bg_image
mode = "bars"                         # Visual mode to useused

[sdl.mode_options]
# When using "bars" mode only these two settings will be used
spacing = 3                           # Spacing between bars and circles
color = "#63c5da"                     # Foreground color

[opengl]
bg_color = "#000000"                  # Background color
spacing = 3                           # Spacing between bars
vert_shader = "./shaders/vertex.vert" # Path to GLSL vertex shader
frag_shader = "./shaders/frag.frag"   # Path to GLSL fragment shader
```

## Known bugs

When using `play_file` the visualization may flicker when the file being played is of low quality 
(few samples). There is currently no fix for this.

## Screenshots

#### Version 0.1.1

![Bars using OpenGL frontend](screenshots/bars_opengl_0_1_1.png)
![Bars using OpenGL frontend 2](screenshots/2_bars_opengl_0_1_1.png)

#### Version 0.1.0

![Bars](screenshots/bars_0_1_0.png)

![Circles](screenshots/circles_0_1_0.png)

![Lines (with circles)](screenshots/lines_0_1_0.png)
