/*
 * bolger - audio visualizer
 * Copyright (C) 2023  marcu5h <marlhan@proton.me>
 *
 * bolger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bolger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bolger.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::{
    process::exit,
    sync::{
        atomic::{AtomicBool, AtomicI8, Ordering},
        mpsc::channel,
        Arc,
    },
    thread::JoinHandle,
};

use bolger::{
    config::{self, parse_config, OutputEnum},
    input::{pipewire, play_file, Input},
    logger,
    output::{self, Output},
};
use log::{debug, error};

static QUIT: AtomicBool = AtomicBool::new(false);

pub fn main() {
    #[cfg(debug_assertions)]
    let log_level = log::LevelFilter::Debug;

    #[cfg(not(debug_assertions))]
    let log_level = log::LevelFilter::Info;

    logger::Blogger::new(log_level)
        .init()
        .expect("Failed to initialize logger");

    let config = match parse_config() {
        Ok(c) => c,
        Err(e) => {
            error!("Error occured parsing config: {e}");
            std::process::exit(1);
        }
    };

    let (sender, receiver) = channel();

    // 0 -> Waiting
    // 1 -> Ok
    // 2 -> Failed
    let input_ok = Arc::new(AtomicI8::from(0));
    #[allow(unused_assignments)]
    let mut input_join_handle: Option<JoinHandle<()>> = None;
    {
        let input_ok = Arc::clone(&input_ok);
        match config.input {
            config::Input::System => {
                input_join_handle = Some(std::thread::spawn(move || {
                    match pipewire::InputPipewire::new(Arc::clone(&input_ok), &sender, &QUIT) {
                        Ok(_) => {}
                        Err(e) => {
                            error!("Failed to create pipewire input: {e}");
                            input_ok.store(2, Ordering::SeqCst);
                        }
                    };
                }));
            }
            config::Input::File(path) => {
                input_join_handle = Some(std::thread::spawn(move || {
                    let input =
                        match play_file::PlayFile::new(Arc::clone(&input_ok), &sender, &QUIT) {
                            Ok(i) => i,
                            Err(e) => {
                                error!("{e}");
                                return;
                            }
                        };

                    match input.init() {
                        Ok(_) => {}
                        Err(e) => {
                            error!("{e}");
                            return;
                        }
                    }

                    match input.run(&path) {
                        Ok(_) => {}
                        Err(e) => {
                            error!("{e}");
                            input_ok.store(2, Ordering::SeqCst);
                        }
                    }
                }));
            }
        }
    }

    debug!("Waiting for input initialization");
    loop {
        match input_ok.load(Ordering::SeqCst) {
            0 => {}
            1 => {
                debug!("Input initialized");
                break;
            }
            2 => {
                error!("Failed to initialize input");
                exit(1);
            }
            _ => unreachable!(),
        }
    }

    let sdl_context = sdl2::init().expect("Failed to init sdl2");

    match config.output {
        OutputEnum::SDL => {
            let mut out =
                match output::sdl::GUISdl::new("bolger", receiver, &QUIT, Some(sdl_context)) {
                    Ok(o) => o,
                    Err(e) => {
                        error!("Failed to create SDL output: {e}");
                        exit(1);
                    }
                };

            out.set_visual_mode(config.sdl.mode);

            out.set_bg_img_path(config.sdl.bg_image.as_deref());
            if let Some((r, g, b)) = config.sdl.bg_image_color_mod {
                out.set_bg_img_color_mod(r, g, b);
            }

            match out.run() {
                Ok(_) => {}
                Err(e) => {
                    QUIT.store(true, std::sync::atomic::Ordering::SeqCst);
                    error!("Error occured running output: {e}");
                }
            }
        }
        OutputEnum::OpenGL => {
            let mut out = match output::opengl::GUIOpenGL::new(
                "bolger",
                receiver,
                &QUIT,
                Some(sdl_context),
            ) {
                Ok(o) => o,
                Err(e) => {
                    error!("Failed to create OpenGL output: {e}");
                    exit(1);
                }
            };

            out.set_bg_color(config.opengl.bg_color);
            out.set_spacing(config.opengl.spacing);
            out.set_shaders_path(config.opengl.vert_shader, config.opengl.frag_shader);

            match out.run() {
                Ok(_) => {}
                Err(e) => {
                    QUIT.store(true, std::sync::atomic::Ordering::SeqCst);
                    error!("Error occured running output: {e}");
                }
            }
        }
    }

    let _ = input_join_handle.unwrap().join();
}
