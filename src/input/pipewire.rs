/*
 * This file is part of bolger
 *
 * Copyright (C) 2023  marcu5h <marlhan@proton.me>
 *
 * bolger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bolger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bolger.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::{
    mem,
    sync::{
        atomic::{AtomicBool, AtomicI8, Ordering},
        mpsc::Sender,
        Arc,
    },
    time::Duration,
};

use log::{debug, error, warn};
use pipewire as pw;
use pw::{
    properties,
    spa::{
        format::{MediaSubtype, MediaType},
        param::{
            audio::{AudioFormat, AudioInfoRaw},
            format_utils, ParamType,
        },
        pod::{serialize::PodSerializer, Object, Pod, Value},
        utils::SpaTypes,
        Direction,
    },
    stream::{Stream, StreamFlags},
    Context, MainLoop,
};
use rustfft::{num_complex::Complex, num_traits::FromPrimitive};

use super::{fft, input_error::InputError, Input, InputData};

const PIPEWIRE_STREAM_NAME: &str = "bolger";

pub struct InputPipewire {}

struct PWUserData<'a> {
    pub sender: &'a Sender<InputData>,
    pub format: AudioInfoRaw,
}

impl<'a> Input<'a> for InputPipewire {
    fn new(
        input_ok: Arc<AtomicI8>,
        sender: &'a Sender<InputData>,
        quit: &'static AtomicBool,
    ) -> Result<Self, InputError> {
        pw::init();

        let mainloop = MainLoop::new()
            .map_err(|e| InputError::new(format!("Failed to create pipewire mainloop: {e}")))?;

        let contex = Context::new(&mainloop)
            .map_err(|e| InputError::new(format!("Failed to create pipewire context: {e}")))?;

        let core = contex
            .connect(None)
            .map_err(|e| InputError::new(format!("Failed to connect to pipewire context: {e}")))?;

        let props = properties! {
            *pw::keys::MEDIA_TYPE => "Audio",
            *pw::keys::MEDIA_CATEGORY => "Capture",
            *pw::keys::MEDIA_ROLE => "Music",
            *pw::keys::STREAM_CAPTURE_SINK => "true",
        };

        let stream = Stream::new(&core, PIPEWIRE_STREAM_NAME, props)
            .map_err(|e| InputError::new(format!("Failed to create pipewire stream: {e}")))?;

        let data = PWUserData {
            sender,
            format: Default::default(),
        };

        let _listener = stream
            .add_local_listener_with_user_data(data)
            .param_changed(|_, id, user_data, param| {
                // NULL means to clear the format
                let Some(param) = param else {
                    return;
                };
                if id != ParamType::Format.as_raw() {
                    return;
                }

                let (media_type, media_subtype) = match format_utils::parse_format(param) {
                    Ok(v) => v,
                    Err(_) => return,
                };

                // only accept raw audio
                if media_type != MediaType::Audio || media_subtype != MediaSubtype::Raw {
                    return;
                }

                // call a helper function to parse the format for us.
                user_data
                    .format
                    .parse(param)
                    .expect("Failed to parse param changed to AudioInfoRaw");

                debug!(
                    "capturing rate:{} channels:{}",
                    user_data.format.rate(),
                    user_data.format.channels()
                );
            })
            .process(|stream, user_data| {
                match stream.dequeue_buffer() {
                    None => warn!("Out of buffers"),
                    Some(mut buffer) => {
                        let datas = buffer.datas_mut();
                        if datas.is_empty() {
                            return;
                        }

                        let data = &mut datas[0];
                        let n_channels = user_data.format.channels();
                        let n_samples = data.chunk().size() / (mem::size_of::<f32>() as u32);
                        if let Some(samples) = data.data() {
                            match n_channels {
                                1 => todo!(), // TODO: quit
                                2 => {
                                    let mut left: Vec<Complex<f32>> =
                                        Vec::with_capacity((n_samples / 2) as usize);
                                    for n in (0..n_samples).step_by(2) {
                                        let start = n as usize * mem::size_of::<f32>();
                                        let end = start + mem::size_of::<f32>();
                                        let chan = &samples[start..end];
                                        left.push(
                                            Complex::from_f32(f32::from_le_bytes(
                                                chan.try_into().unwrap(),
                                            ))
                                            .unwrap(),
                                        );
                                    }

                                    let mut right: Vec<Complex<f32>> =
                                        Vec::with_capacity((n_samples / 2) as usize);
                                    for n in (1..n_samples).step_by(2) {
                                        let start = n as usize * mem::size_of::<f32>();
                                        let end = start + mem::size_of::<f32>();
                                        let chan = &samples[start..end];
                                        right.push(
                                            Complex::from_f32(f32::from_le_bytes(
                                                chan.try_into().unwrap(),
                                            ))
                                            .unwrap(),
                                        );
                                    }

                                    // let start = Instant::now();
                                    let heights =
                                        fft::from_stereo(&mut left, &mut right, n_samples);
                                    // println!("fft took {}µs", start.elapsed().as_micros());
                                    if let Err(e) =
                                        user_data.sender.send(InputData::Heights(heights))
                                    {
                                        error!("Failed to send to output: {e}");
                                    }
                                }
                                _ => todo!(), // TODO: quit
                            }
                        }
                    }
                }
            })
            .register()
            .map_err(|e| InputError::new(format!("Failed to create pipewire listener: {e}")))?;

        let mut audio_info = AudioInfoRaw::new();
        audio_info.set_format(AudioFormat::F32LE);
        let obj = Object {
            type_: SpaTypes::ObjectParamFormat.as_raw(),
            id: ParamType::EnumFormat.as_raw(),
            properties: audio_info.into(),
        };
        let values: Vec<u8> =
            PodSerializer::serialize(std::io::Cursor::new(Vec::new()), &Value::Object(obj))
                .map_err(|e| InputError::new(format!("Failed to deserialize obj: {e}")))?
                .0
                .into_inner();

        let mut params = [match Pod::from_bytes(&values) {
            Some(p) => p,
            None => {
                return Err(InputError::new("Failed get params".to_string()));
            }
        }];

        stream
            .connect(
                Direction::Input,
                None,
                StreamFlags::AUTOCONNECT | StreamFlags::MAP_BUFFERS | StreamFlags::RT_PROCESS,
                &mut params,
            )
            .map_err(|e| InputError::new(format!("Failed to connect to stream: {e}")))?;

        // Ok(Self { mainloop, quit, stream })
        input_ok.store(1, Ordering::SeqCst);

        while !quit.load(Ordering::SeqCst) {
            mainloop.iterate(Duration::from_millis(10));
        }

        Ok(Self {})
    }
}
