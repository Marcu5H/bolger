/*
 * This file is part of bolger
 *
 * Copyright (C) 2023  marcu5h <marlhan@proton.me>
 *
 * bolger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bolger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bolger.  If not, see <https://www.gnu.org/licenses/>.
 */

use rustfft::{
    algorithm::Radix4,
    num_complex::{Complex, Complex32, ComplexFloat},
    Fft, FftDirection, FftNum, FftPlanner,
};

pub fn from_stereo(l: &mut Vec<Complex<f32>>, r: &mut [Complex<f32>], n_samples: u32) -> Vec<f32> {
    let fft = Radix4::new((n_samples / 2) as usize, FftDirection::Forward);

    fft.process(l);
    fft.process(r);

    // TODO: SIMD

    let mut stereo_avg: Vec<f32> = Vec::new();
    let mut i = 0;
    while i < l.len() / 2 {
        stereo_avg.push((l[i].abs() + r[i].abs()) / 2.0);
        i += 1;
    }

    let mut max = 0.0;
    for a in &stereo_avg {
        if a > &max {
            max = *a;
        }
    }

    i = 0;
    while i < stereo_avg.len() {
        stereo_avg[i] /= max;
        i += 1;
    }

    stereo_avg
}

pub fn new_planner<T>() -> FftPlanner<T>
where
    T: FftNum,
{
    FftPlanner::<T>::new()
}

pub fn from_mono_with_planner(
    planner: &mut FftPlanner<f32>,
    samples: &mut Vec<Complex32>,
    n_samples: u32,
) -> Vec<f32> {
    let fft = planner.plan_fft_forward(n_samples as usize);
    fft.process(samples);

    let mut max = 0.0;
    for a in &mut *samples {
        if a.abs() > max {
            max = a.abs();
        }
    }

    let mut heights: Vec<f32> = Vec::with_capacity(n_samples as usize / 2);
    heights.resize(n_samples as usize / 2, 0.0);
    // for _ in 0..n_samples / 2 {
    //     heights.push(0.0);
    // }

    let mut i = 0;
    while i < heights.len() {
        heights[i] = samples[i].abs() / max;
        i += 1;
    }

    heights
}
