/*
 * This file is part of bolger
 *
 * Copyright (C) 2023  marcu5h <marlhan@proton.me>
 *
 * bolger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bolger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bolger.  If not, see <https://www.gnu.org/licenses/>.
 */

mod fft;
pub mod input_error;
pub mod pipewire;
pub mod play_file;

use std::sync::{
    atomic::{AtomicBool, AtomicI8},
    mpsc::Sender,
    Arc,
};

use self::input_error::InputError;

pub enum InputData {
    Heights(Vec<f32>),
    Quit,
}

pub trait Input<'a> {
    fn new(
        input_ok: Arc<AtomicI8>,
        sender: &'a Sender<InputData>,
        quit: &'static AtomicBool,
    ) -> Result<Self, InputError>
    where
        Self: Sized;
}
