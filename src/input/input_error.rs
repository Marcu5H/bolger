/*
 * This file is part of bolger
 *
 * Copyright (C) 2023  marcu5h <marlhan@proton.me>
 *
 * bolger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bolger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bolger.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::{
    error::Error,
    fmt::{self, Display, Formatter},
};

use super::play_file::{self, ErrorMessage};

#[derive(Debug)]
pub struct InputError {
    msg: String,
}

impl Error for InputError {}

impl InputError {
    pub fn new(msg: String) -> Self {
        Self { msg }
    }
}

impl From<play_file::ErrorMessage> for InputError {
    fn from(e: ErrorMessage) -> Self {
        Self::new(format!("{}", e.error))
    }
}

impl Display for InputError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.msg)
    }
}
