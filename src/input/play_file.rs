/*
 * This file is part of bolger
 *
 * Copyright (C) 2023  marcu5h <marlhan@proton.me>
 *
 * bolger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bolger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bolger.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::{
    path::Path,
    sync::{
        atomic::{AtomicBool, AtomicI8},
        mpsc::Sender,
        Arc, Mutex,
    },
};

use gst::{
    element_error, element_warning,
    glib::{self, Cast, ObjectExt},
    prelude::{ElementExt, GstBinExtManual, GstObjectExt, PadExt},
    MessageView,
};
use log::{debug, error};

use rustfft::{num_complex::Complex, num_traits::FromPrimitive, FftPlanner};

use crate::input::fft;

use byte_slice_cast::*;

use super::{fft::new_planner, input_error::InputError, Input, InputData};

pub struct PlayFile<'a> {
    input_ok: Arc<AtomicI8>,
    sender: &'a Sender<InputData>,
    quit: &'static AtomicBool,
}

use gstreamer as gst;
use gstreamer_app as gst_app;
use gstreamer_audio as gst_audio;

use derive_more::{Display, Error};

#[derive(Debug, Display, Error)]
#[display(fmt = "Received error from {src}: {error} (debug: {debug:?})")]
pub struct ErrorMessage {
    pub src: glib::GString,
    pub error: glib::Error,
    pub debug: Option<glib::GString>,
}

#[derive(Clone, Debug, gst::glib::Boxed)]
#[boxed_type(name = "ErrorValue")]
struct ErrorValue(Arc<Mutex<Option<InputError>>>);

impl<'a> Input<'a> for PlayFile<'a> {
    fn new(
        input_ok: Arc<AtomicI8>,
        sender: &'a Sender<InputData>,
        quit: &'static AtomicBool,
    ) -> Result<Self, InputError> {
        Ok(Self {
            input_ok,
            sender,
            quit,
        })
    }
}

impl<'a> PlayFile<'a> {
    pub fn init(&self) -> Result<(), InputError> {
        gst::init().map_err(|e| InputError::new(format!("Failed to initialize gstreamer: {e}")))?;
        Ok(())
    }

    pub fn run(&self, audio_file: &Path) -> Result<(), InputError> {
        if !audio_file.exists() {
            return Err(InputError::new(format!(
                "Provided file does not exits ({audio_file:?})"
            )));
        }

        let pipeline = gst::Pipeline::default();
        let src = gst::ElementFactory::make("filesrc")
            .property("location", audio_file)
            .build()
            .map_err(|e| {
                InputError::new(format!("Failed to create gstreamer source element: {e}"))
            })?;
        let decodebin = gst::ElementFactory::make("decodebin")
            .build()
            .map_err(|e| {
                InputError::new(format!("Failed to create gstreamer decodebin element: {e}"))
            })?;

        pipeline.add_many([&src, &decodebin]).map_err(|e| {
            InputError::new(format!("Failed to add elements to gstreamer pipeline: {e}"))
        })?;
        gst::Element::link_many([&src, &decodebin])
            .map_err(|e| InputError::new(format!("Failed to link gstreamer elements: {e}")))?;

        let appsink = gst_app::AppSink::builder()
            .caps(
                &gst_audio::AudioCapsBuilder::new_interleaved()
                    .format(gst_audio::AudioFormat::F32le)
                    .channels(2)
                    .build(),
            )
            .build();
        appsink.set_property("emit-signals", true);

        let sender = self.sender.clone();
        let mut fft_planner: FftPlanner<f32> = new_planner();
        appsink.set_callbacks(
            gst_app::AppSinkCallbacks::builder()
                .new_sample(move |appsink| {
                    let sample = appsink.pull_sample().map_err(|_| gst::FlowError::Eos)?;
                    let buffer = sample.buffer().ok_or_else(|| {
                        element_error!(
                            appsink,
                            gst::ResourceError::Failed,
                            ("Failed to get buffer from appsink")
                        );

                        gst::FlowError::Error
                    })?;

                    let map = buffer.map_readable().map_err(|_| {
                        element_error!(
                            appsink,
                            gst::ResourceError::Failed,
                            ("Failed to map buffer readable")
                        );

                        gst::FlowError::Error
                    })?;

                    let samples = map.as_slice_of::<f32>().map_err(|_| {
                        element_error!(
                            appsink,
                            gst::ResourceError::Failed,
                            ("Failed to interpret buffer as F32 PCM")
                        );

                        gst::FlowError::Error
                    })?;

                    let mut c_samples: Vec<Complex<f32>> = Vec::new(); // TODO: Make helper functions for conversion from f32 to complex<f32>
                    for i in (1..samples.len()).step_by(2) {
                        c_samples.push(
                            Complex::from_f32((samples[i] + samples[i - 1]) / 2.0).expect("TODO"),
                        );
                    }
                    let n_samples = c_samples.len();
                    match sender.send(InputData::Heights(fft::from_mono_with_planner(
                        &mut fft_planner,
                        &mut c_samples,
                        n_samples as u32,
                    ))) {
                        Ok(_) => {}
                        Err(e) => {
                            error!("Failed to send heights to ouput: {e}");
                        }
                    }

                    Ok(gst::FlowSuccess::Ok)
                })
                .build(),
        );

        let pipeline_weak = pipeline.downgrade();
        decodebin.connect_pad_added(move |dbin, src_pad| {
            let Some(pipeline) = pipeline_weak.upgrade() else {
                return;
            };

            let (is_audio, is_video) = {
                let media_type = src_pad.current_caps().and_then(|caps| {
                    caps.structure(0).map(|s| {
                        let name = s.name();
                        (name.starts_with("audio/"), name.starts_with("video/"))
                    })
                });

                match media_type {
                    None => {
                        element_warning!(
                            dbin,
                            gst::CoreError::Negotiation,
                            ("Failed to get media type from pad {}", src_pad.name())
                        );

                        return;
                    }
                    Some(media_type) => media_type,
                }
            };

            let insert_sink = |is_audio, is_video| -> Result<(), InputError> {
                if is_audio {
                    let queue = gst::ElementFactory::make("queue").build().map_err(|e| {
                        InputError::new(format!("Failed to make queue element: {e}"))
                    })?;
                    let queue2 = gst::ElementFactory::make("queue").build().map_err(|e| {
                        InputError::new(format!("Failed to make queue element: {e}"))
                    })?;
                    let convert =
                        gst::ElementFactory::make("audioconvert")
                            .build()
                            .map_err(|e| {
                                InputError::new(format!("Failed to make convert element: {e}"))
                            })?;
                    let convert2 =
                        gst::ElementFactory::make("audioconvert")
                            .build()
                            .map_err(|e| {
                                InputError::new(format!("Failed to make convert element: {e}"))
                            })?;
                    let tee = gst::ElementFactory::make("tee")
                        .build()
                        .map_err(|e| InputError::new(format!("Failed to make tee element: {e}")))?;
                    let resample =
                        gst::ElementFactory::make("audioresample")
                            .build()
                            .map_err(|e| {
                                InputError::new(format!("Failed to make resample element: {e}"))
                            })?;
                    let sink = gst::ElementFactory::make("autoaudiosink")
                        .build()
                        .map_err(|e| {
                            InputError::new(format!("Failed to make sink element: {e}"))
                        })?;

                    let elements = &[
                        &queue,
                        &queue2,
                        &convert,
                        &convert2,
                        &resample,
                        &tee,
                        &sink,
                        appsink.upcast_ref(),
                    ];
                    pipeline.add_many(elements).map_err(|e| {
                        InputError::new(format!("Failed to add elements to pipeline: {e}"))
                    })?;
                    gst::Element::link_many([&tee, &queue, &convert, appsink.upcast_ref()])
                        .map_err(|e| InputError::new(format!("Failed to link elements: {e}")))?;
                    gst::Element::link_many([&tee, &queue2, &convert2, &resample, &sink]).map_err(
                        |e| {
                            InputError::new(format!("Failed to link tee and appsink elements: {e}"))
                        },
                    )?;

                    for e in elements {
                        e.sync_state_with_parent().map_err(|e| {
                            InputError::new(format!("Failed to sync element with parent: {e}"))
                        })?;
                    }

                    let sink_pad = match tee.static_pad("sink") {
                        Some(p) => p,
                        None => {
                            return Err(InputError::new("Queue has no sinkpad".to_string()));
                        }
                    };
                    src_pad.link(&sink_pad).map_err(|e| {
                        InputError::new(format!("Failed to link sink pad to src pad: {e}"))
                    })?;
                } else if is_video {
                    debug!("Got video stream. Discarding");
                }

                Ok(())
            };

            if let Err(err) = insert_sink(is_audio, is_video) {
                // The following sends a message of type Error on the bus, containing our detailed
                // error information.
                element_error!(
                    dbin,
                    gst::LibraryError::Failed,
                    ("Failed to insert sink"),
                    details: gst::Structure::builder("error-details")
                                .field("error",
                                    &ErrorValue(Arc::new(Mutex::new(Some(err)))))
                                .build()
                );
            }
        });

        pipeline
            .set_state(gst::State::Playing)
            .map_err(|e| InputError::new(format!("Failed to set pipeline to Playing: {e}")))?;

        let bus = match pipeline.bus() {
            Some(b) => b,
            None => {
                return Err(InputError::new("Failed get pipeline bus".to_string()));
            }
        };

        self.input_ok.store(1, std::sync::atomic::Ordering::SeqCst);

        loop {
            let msg = match bus.timed_pop(gst::ClockTime::from_mseconds(250)) {
                Some(m) => m,
                None => {
                    if self.quit.load(std::sync::atomic::Ordering::SeqCst) {
                        debug!("self.quit = true");
                        break;
                    }
                    continue;
                }
            };
            match msg.view() {
                MessageView::Eos(..) => {
                    self.sender.send(InputData::Quit).map_err(|e| {
                        InputError::new(format!("Failed to send quit message to ouput: {e}"))
                    })?;
                    break;
                }
                // TODO: Send quit to output
                MessageView::Error(err) => {
                    pipeline.set_state(gst::State::Null).map_err(|e| {
                        InputError::new(format!("Failed to set pipeline state to Null: {e}"))
                    })?;

                    match err.details() {
                        Some(details) if details.name() == "error-details" => details
                            .get::<&ErrorValue>("error")
                            .unwrap()
                            .clone()
                            .0
                            .lock()
                            .unwrap()
                            .take()
                            .map(Result::Err)
                            .expect("error-details message without actual error"), // TODO: handle error
                        _ => Err(ErrorMessage {
                            src: msg
                                .src()
                                .map(|s| s.path_string())
                                .unwrap_or_else(|| glib::GString::from("UNKNOWN")),
                            error: err.error(),
                            debug: err.debug(),
                        }
                        .into()),
                    }?;
                }
                MessageView::StateChanged(s) => {
                    debug!(
                        "State changed from {:?}: {:?} -> {:?} ({:?})",
                        s.src().map(|s| s.path_string()),
                        s.old(),
                        s.current(),
                        s.pending()
                    );
                }
                _ => {}
            }
        }

        pipeline
            .set_state(gst::State::Null)
            .map_err(|e| InputError::new(format!("Failed to set pipeline state to Null: {e}")))?;

        Ok(())
    }
}
