use log::{Level, LevelFilter, Log, Record, SetLoggerError};

const ANSI_RED: &str = "\x1b[31m";
const ANSI_YELLOW: &str = "\x1b[33m";
const ANSI_GREEN: &str = "\x1b[32m";
const ANSI_BLUE: &str = "\x1b[34m";
const ANSI_CYAN: &str = "\x1b[36m";
const ANSI_CLEAR: &str = "\x1b[0m";

pub struct Blogger {
    filter: LevelFilter,
}

impl Blogger {
    pub fn new(filter: LevelFilter) -> Self {
        Self { filter }
    }

    pub fn init(self) -> Result<(), SetLoggerError> {
        log::set_max_level(self.filter);
        log::set_boxed_logger(Box::new(self))
    }

    pub fn is_enabled(&self, level: Level) -> bool {
        let filter_level = match self.filter.to_level() {
            Some(l) => l,
            None => return false,
        };

        level <= filter_level
    }
}

impl Log for Blogger {
    fn enabled(&self, metadata: &log::Metadata) -> bool {
        self.is_enabled(metadata.level())
    }

    fn log(&self, record: &Record<'_>) {
        if !self.enabled(record.metadata()) {
            return;
        }

        let level_label = match record.level() {
            Level::Error => format!("{ANSI_RED}ERROR{ANSI_CLEAR}"),
            Level::Warn => format!("{ANSI_YELLOW}WARN{ANSI_CLEAR}"),
            Level::Info => format!("{ANSI_GREEN}INFO{ANSI_CLEAR}"),
            Level::Debug => format!("{ANSI_BLUE}DEBUG{ANSI_CLEAR}"),
            Level::Trace => format!("{ANSI_CYAN}TRACE{ANSI_CLEAR}"),
        };

        eprintln!(
            "{level_label} {}(L:{}) | {}",
            record.module_path().unwrap_or("n/a"),
            record.line().unwrap_or(0),
            record.args(),
        );
    }

    fn flush(&self) {}
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn log_level_trace() {
        let logger = Blogger::new(LevelFilter::Trace);
        assert_eq!(logger.is_enabled(Level::Debug), true);
        assert_eq!(logger.is_enabled(Level::Info), true);
        assert_eq!(logger.is_enabled(Level::Warn), true);
        assert_eq!(logger.is_enabled(Level::Error), true);
    }

    #[test]
    fn log_level_warn() {
        let logger = Blogger::new(LevelFilter::Warn);
        assert_eq!(logger.is_enabled(Level::Debug), false);
        assert_eq!(logger.is_enabled(Level::Info), false);
        assert_eq!(logger.is_enabled(Level::Warn), true);
        assert_eq!(logger.is_enabled(Level::Error), true);
    }
}
