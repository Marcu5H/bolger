use std::{ffi::CString, fs::File, io::Read, path::PathBuf};

use gl::types::{GLchar, GLenum, GLint, GLuint};

use crate::output::output_error::OutputError;

pub struct Shader {
    code: String,
    id: GLuint,
}

impl Shader {
    pub fn from_file(path: PathBuf) -> Result<Self, OutputError> {
        if !path.exists() {
            return Err(OutputError::new(format!(
                "Provided shader {path:?} does not exist"
            )));
        }

        let mut file = File::open(&path)
            .map_err(|e| OutputError::new(format!("Failed to open shader file: {e}")))?;

        let mut code = String::new();
        file.read_to_string(&mut code)
            .map_err(|e| OutputError::new(format!("Failed to read from shader file: {e}")))?;

        Ok(Self { code, id: 0 })
    }

    pub fn compile(mut self, type_: GLenum) -> Result<Self, OutputError> {
        unsafe {
            self.id = gl::CreateShader(type_);

            let code_c_str = CString::new(self.code.as_bytes())
                .map_err(|e| OutputError::new(format!("Failed to make CString from code: {e}")))?;
            gl::ShaderSource(self.id, 1, &code_c_str.as_ptr(), std::ptr::null());
            gl::CompileShader(self.id);

            let mut status = gl::FALSE as GLint;
            gl::GetShaderiv(self.id, gl::COMPILE_STATUS, &mut status);

            if status != (gl::TRUE as GLint) {
                let mut len = 0;
                gl::GetShaderiv(self.id, gl::INFO_LOG_LENGTH, &mut len);
                let mut buf = Vec::with_capacity(len as usize);
                let _ = buf.spare_capacity_mut();
                buf.set_len((len as usize) - 1); // subtract 1 to skip the trailing null character
                gl::GetShaderInfoLog(
                    self.id,
                    len,
                    std::ptr::null_mut(),
                    buf.as_mut_ptr() as *mut GLchar,
                );
                return Err(OutputError::new(format!(
                    "Failed to compile shader ({}): {}",
                    self.code,
                    std::str::from_utf8(&buf).ok().unwrap_or("n/a")
                )));
            }
        }

        Ok(self)
    }

    pub fn get_id(&self) -> GLuint {
        self.id
    }
}
