/*
 * This file is part of bolger
 *
 * Copyright (C) 2023  marcu5h <marlhan@proton.me>
 *
 * bolger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bolger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bolger.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::{
    cmp::Ordering,
    path::Path,
    sync::{
        atomic::AtomicBool,
        mpsc::{Receiver, RecvTimeoutError},
    },
    time::Duration,
};

use log::debug;
use ndarray::Array;
use sdl2::{
    event::{Event, WindowEvent},
    gfx::primitives::DrawRenderer,
    image::{self, InitFlag, LoadTexture},
    keyboard::Keycode,
    pixels::Color,
    rect::{Point, Rect},
    render::Canvas,
    video::Window,
    Sdl,
};

use crate::input::InputData;

use super::{output_error::OutputError, Output};

#[derive(PartialEq)]
pub struct Bars {
    pub spacing: i32,
    pub bar_color: Color,
}

#[derive(PartialEq)]
pub struct Circles {
    pub spacing: i32,
    pub circle_color: Color,
}

#[derive(PartialEq)]
pub struct Lines {
    pub line_color: Color,
    pub show_point: bool,
    pub point_color: Color,
    pub point_radius: i16,
}

#[derive(PartialEq)]
pub enum VisualMode {
    Bars(Bars),
    Circles(Circles),
    Lines(Lines),
    NotSet,
}

pub struct GUISdl<'a> {
    sdl_context: Sdl,
    canvas: Canvas<Window>,
    input_quit: &'static AtomicBool,
    data_receiver: Receiver<InputData>,
    width: u32,
    height: u32,
    visual_mode: VisualMode,
    bg_img_path: Option<&'a Path>,
    bg_img_color_mod: Option<(u8, u8, u8)>,
    bg_color: Color,
    point_count: usize,
    last_heights: Vec<u32>,
    last_heights_len: usize,
    smoothing_divisor: u32,
    indexes: (Vec<usize>, usize, usize),
}

impl<'a> Output<'a> for GUISdl<'a> {
    fn new(
        title: &'a str,
        data_receiver: Receiver<InputData>,
        input_quit: &'static AtomicBool,
        sdl_context: Option<Sdl>,
    ) -> Result<Self, OutputError> {
        let sdl_context = match sdl_context {
            Some(c) => c,
            None => return Err(OutputError::new("No sdl_context provided".to_string())),
        };

        let video_subsystem = sdl_context
            .video()
            .map_err(|e| OutputError::new(format!("Failed to get video subsystem: {e}")))?;

        let window = video_subsystem
            .window(title, 1000, 1000)
            .resizable()
            .build()
            .map_err(|e| OutputError::new(format!("Failed to create window: {e}")))?;

        let (width, height) = window.size();

        let canvas = window
            .into_canvas()
            .build()
            .map_err(|e| OutputError::new(format!("Failed to create window: {e}")))?;

        Ok(Self {
            sdl_context,
            canvas,
            input_quit,
            data_receiver,
            width,
            height,
            visual_mode: VisualMode::NotSet,
            bg_img_path: None,
            bg_img_color_mod: None,
            bg_color: Color::RGB(0, 0, 0),
            point_count: 32,
            last_heights: Vec::new(),
            last_heights_len: 0,
            smoothing_divisor: 8,
            indexes: (Vec::new(), 0, 0),
        })
    }

    fn run(&mut self) -> Result<(), OutputError> {
        if self.visual_mode == VisualMode::NotSet {
            return Err(OutputError::new("visual_mode is not set".to_string()));
        }

        self.canvas.set_draw_color(Color::RGB(255, 255, 255));
        self.canvas.clear();
        self.canvas.present();
        let texture_creator = self.canvas.texture_creator();
        let mut background_image = None;
        if let Some(path) = self.bg_img_path {
            let _image_context = image::init(InitFlag::PNG | InitFlag::JPG)
                .map_err(|e| OutputError::new(format!("Failed to create image context: {e}")))?;
            let mut img_texture = texture_creator
                .load_texture(path)
                .map_err(|e| OutputError::new(format!("Failed to load image: {e}")))?;
            if let Some((r, g, b)) = self.bg_img_color_mod {
                img_texture.set_color_mod(r, g, b);
            }
            background_image = Some(img_texture);
        }

        let mut event_pump = self.sdl_context.event_pump().unwrap();
        'running: loop {
            if let Some(img_texture) = &background_image {
                let _ = self.canvas.copy(img_texture, None, None);
            } else {
                self.canvas.set_draw_color(Color::RGB(0, 0, 0)); // TODO: custom
                self.canvas.clear();
            }

            match self.data_receiver.recv_timeout(Duration::from_millis(100)) {
                Ok(d) => match d {
                    InputData::Heights(h) => match &self.visual_mode {
                        VisualMode::Bars(_) => {
                            self.draw_bars(&h);
                        }
                        VisualMode::Circles(_) => {
                            self.draw_circles(&h);
                        }
                        VisualMode::Lines(_) => {
                            self.draw_lines(&h);
                        }
                        VisualMode::NotSet => {
                            self.input_quit
                                .store(true, std::sync::atomic::Ordering::SeqCst);
                            return Err(OutputError::new("visual_mode is not set".to_string()));
                        }
                    },
                    InputData::Quit => {
                        debug!("Received quit message");
                        break 'running;
                    }
                },
                Err(e) => {
                    if e == RecvTimeoutError::Disconnected {
                        self.input_quit
                            .store(true, std::sync::atomic::Ordering::SeqCst);
                        return Err(OutputError::new("Input channel disconnected".to_string()));
                    }
                }
            }

            self.canvas.present();

            for event in event_pump.poll_iter() {
                match event {
                    Event::Quit { .. }
                    | Event::KeyDown {
                        keycode: Some(Keycode::Escape),
                        ..
                    } => break 'running,
                    Event::Window {
                        win_event: WindowEvent::Resized(w, h),
                        ..
                    } => {
                        self.width = w as u32;
                        self.height = h as u32;
                    }
                    Event::KeyDown {
                        keycode: Some(Keycode::Plus),
                        ..
                    } => {
                        if self.point_count <= 128 {
                            // Max limit of 256
                            self.point_count *= 2;
                        }
                    }
                    Event::KeyDown {
                        keycode: Some(Keycode::Minus),
                        ..
                    } => {
                        if self.point_count >= 4 {
                            // Min limit of 2
                            self.point_count /= 2;
                        }
                    }
                    _ => {}
                }
            }
        }

        self.input_quit
            .store(true, std::sync::atomic::Ordering::SeqCst);

        Ok(())
    }
}

impl<'a> GUISdl<'a> {
    pub fn set_visual_mode(&mut self, mode: VisualMode) {
        self.visual_mode = mode;
    }

    pub fn set_bg_img_path(&mut self, path: Option<&'a Path>) {
        self.bg_img_path = path;
    }

    pub fn set_bg_img_color_mod(&mut self, red: u8, green: u8, blue: u8) {
        self.bg_img_color_mod = Some((red, green, blue));
    }

    pub fn set_bg_color(&mut self, color: Color) {
        self.bg_color = color;
    }

    #[inline(always)]
    fn calculate_heights(&mut self, h: &Vec<f32>) -> u32 {
        let mut heights: Vec<u32> = Vec::new();
        let mut update_heights = false;
        // Update the cached logspace for frequency indexes
        if h.len() != self.indexes.1 || self.point_count != self.indexes.2 {
            if self.point_count != self.indexes.2 {
                self.indexes.2 = self.point_count;
                update_heights = true;
            }
            let m = h.len().ilog2() as f32;
            self.indexes.0 = Array::logspace(2.0, 0.0, m, self.point_count)
                .map(|n| (*n).ceil() as usize)
                .to_vec();
            self.indexes.1 = h.len();
        }

        let mut c = 0;
        let mut point_count = 0;
        for _i in 0..self.indexes.0.len() {
            let i = self.indexes.0[_i];
            if _i > 1 {
                // Skip duplicating bars for a nicer visualizing
                // This will reduce the total amount of points
                if self.indexes.0[_i - 1] == self.indexes.0[_i] {
                    continue;
                }
            }

            let mut sum: u32 = 0;
            let x = i - c;
            // Calculate average of heights between last and current index
            while c < i {
                sum += (h[c] * self.height as f32) as u32;
                c += 1;
            }

            point_count += 1;
            // Store the average height for the interval
            heights.push(
                (if x < 1 {
                    (h[c] * self.height as f32) as u32
                } else {
                    sum / x as u32
                })
                .clamp(0, self.height),
            );
        }

        if update_heights
            || (self.last_heights_len != h.len() && self.last_heights.len() != heights.len())
        {
            self.last_heights = heights;
            self.last_heights_len = h.len();
        } else {
            let mut i = 0;
            while i < self.last_heights.len() {
                // Smoothing
                match self.last_heights[i].cmp(&heights[i]) {
                    Ordering::Less => {
                        self.last_heights[i] +=
                            (heights[i] - self.last_heights[i]) / self.smoothing_divisor;
                    }
                    Ordering::Greater => {
                        self.last_heights[i] -=
                            (self.last_heights[i] - heights[i]) / (self.smoothing_divisor * 2) + 1;
                    }
                    _ => {}
                }
                i += 1;
            }
        }

        point_count
    }

    fn draw_bars(&mut self, h: &Vec<f32>) {
        let bar_count = self.calculate_heights(h);

        if let VisualMode::Bars(bars) = &self.visual_mode {
            let bar_width = (self.width - bars.spacing as u32 * bar_count) / bar_count;

            let mut rects: Vec<Rect> = Vec::new();
            let mut x = 0;
            for height in &self.last_heights {
                let y = (self.height - height.clamp(&0, &self.height)) as i32;
                rects.push(Rect::new(x, y, bar_width, *height));
                x += bar_width as i32 + bars.spacing;
            }

            self.canvas.set_draw_color(bars.bar_color);
            let _ = self.canvas.fill_rects(&rects); // TODO: Handle errors
        }
    }

    fn draw_circles(&mut self, h: &Vec<f32>) {
        let circle_count = self.calculate_heights(h);
        if let VisualMode::Circles(circles) = &self.visual_mode {
            let circle_radius =
                ((self.width - circles.spacing as u32 * circle_count) / circle_count) / 2;

            let mut x = circle_radius as i32;
            for height in &self.last_heights {
                let y = (self.height - height.clamp(&0, &self.height)) as i32;
                let _ = self.canvas.filled_circle(
                    x as i16,
                    y as i16,
                    circle_radius as i16,
                    circles.circle_color,
                );
                x += (circle_radius * 2) as i32 + circles.spacing;
            }
        }
    }

    fn draw_lines(&mut self, h: &Vec<f32>) {
        let circle_count = self.calculate_heights(h);
        if let VisualMode::Lines(lines) = &self.visual_mode {
            let point_radius = ((self.width) / circle_count) / 2;
            let mut x = 0;
            let mut points: Vec<Point> = Vec::new();
            for height in &self.last_heights {
                let y = (self.height - height.clamp(&0, &self.height)) as i32;
                points.push(Point::new(x, y));
                if lines.show_point {
                    let _ = self.canvas.filled_circle(
                        x as i16,
                        y as i16,
                        lines.point_radius,
                        lines.point_color,
                    );
                }
                x += (point_radius * 2) as i32;
            }
            self.canvas.set_draw_color(lines.line_color);
            let _ = self.canvas.draw_lines(points.as_slice());
        }
    }
}
