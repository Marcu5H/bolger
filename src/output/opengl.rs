/*
 * This file is part of bolger
 *
 * Copyright (C) 2023  marcu5h <marlhan@proton.me>
 *
 * bolger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bolger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bolger.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::{
    ffi::{c_void, CString},
    path::PathBuf,
    sync::{
        atomic::AtomicBool,
        mpsc::{Receiver, RecvTimeoutError},
    },
    time::Duration,
};

use gl::types::{GLchar, GLfloat, GLint, GLsizeiptr, GLuint};
use log::{debug, warn};
use ndarray::Array;
use sdl2::{
    event::{Event, WindowEvent},
    keyboard::Keycode,
    video::{GLContext, Window},
    Sdl,
};

use crate::input::InputData;

mod shader;

use self::shader::Shader;

use super::{output_error::OutputError, Output};

fn link_program(vs: GLuint, fs: GLuint) -> Result<GLuint, OutputError> {
    unsafe {
        let program = gl::CreateProgram();
        gl::AttachShader(program, vs);
        gl::AttachShader(program, fs);
        gl::LinkProgram(program);
        // Get the link status
        let mut status = gl::FALSE as GLint;
        gl::GetProgramiv(program, gl::LINK_STATUS, &mut status);

        // Fail on error
        if status != (gl::TRUE as GLint) {
            let mut len: GLint = 0;
            gl::GetProgramiv(program, gl::INFO_LOG_LENGTH, &mut len);
            let mut buf = Vec::with_capacity(len as usize);
            let _ = buf.spare_capacity_mut();
            buf.set_len((len as usize) - 1); // subtract 1 to skip the trailing null character
            gl::GetProgramInfoLog(
                program,
                len,
                std::ptr::null_mut(),
                buf.as_mut_ptr() as *mut GLchar,
            );
            return Err(OutputError::new(format!(
                "Failed to link program: {}",
                std::str::from_utf8(&buf).ok().unwrap_or("n/a")
            )));
        }
        Ok(program)
    }
}

pub struct GUIOpenGL {
    sdl_context: Sdl,
    #[allow(dead_code)]
    glctx: GLContext,
    window: Window,
    input_quit: &'static AtomicBool,
    data_receiver: Receiver<InputData>,
    width: i32,
    height: i32,
    bg_color: (f32, f32, f32),
    point_count: usize,
    last_heights: Vec<f32>,
    last_heights_len: usize,
    smoothing_divisor: f32,
    indexes: (Vec<usize>, usize, usize),
    spacing: u32,
    vert_shader_path: Option<PathBuf>,
    frag_shader_path: Option<PathBuf>,
}

impl<'a> Output<'a> for GUIOpenGL {
    fn new(
        title: &'a str,
        data_receiver: Receiver<InputData>,
        input_quit: &'static AtomicBool,
        sdl_context: Option<Sdl>,
    ) -> Result<Self, OutputError> {
        let sdl_context = match sdl_context {
            Some(c) => c,
            None => return Err(OutputError::new("No sdl_context provided".to_string())),
        };

        let video_subsystem = sdl_context
            .video()
            .map_err(|e| OutputError::new(format!("Failed to get video subsystem: {e}")))?;

        let gl_attr = video_subsystem.gl_attr();
        gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
        gl_attr.set_context_version(3, 3);

        let window = video_subsystem
            .window(title, 1000, 1000)
            .resizable()
            .opengl()
            .build()
            .map_err(|e| OutputError::new(format!("Failed to create window: {e}")))?;

        let (width, height) = window.size();

        let glctx = window
            .gl_create_context()
            .map_err(|e| OutputError::new(format!("Failed to create GL context: {e}")))?;

        video_subsystem
            .gl_set_swap_interval(1)
            .map_err(|e| OutputError::new(format!("Failed to enable vsync: {e}")))?;

        gl::load_with(|name| video_subsystem.gl_get_proc_address(name) as *const _);

        Ok(Self {
            sdl_context,
            glctx,
            window,
            input_quit,
            data_receiver,
            width: width as i32,
            height: height as i32,
            bg_color: (0.0, 0.0, 0.0),
            point_count: 32,
            last_heights: Vec::new(),
            last_heights_len: 0,
            smoothing_divisor: 6.0,
            indexes: (Vec::new(), 0, 0),
            spacing: 3,
            vert_shader_path: None,
            frag_shader_path: None,
        })
    }

    fn run(&mut self) -> Result<(), OutputError> {
        let vertices: Vec<GLfloat> = vec![
            -1.0, 1.0, -0.1, -1.0, -1.0, -1.0, -1.0, 1.0, -0.1, -1.0, -0.1, 1.0,
        ];

        // let vertex_shader
        let vertex_shader = Shader::from_file(PathBuf::from(match &self.vert_shader_path {
            Some(path) => path,
            None => {
                return Err(OutputError::new("No vertex shader provided".to_string()));
            }
        }))?
        .compile(gl::VERTEX_SHADER)?;
        let fragment_shader = Shader::from_file(PathBuf::from(match &self.frag_shader_path {
            Some(path) => path,
            None => {
                return Err(OutputError::new("No fragment shader provided".to_string()));
            }
        }))?
        .compile(gl::FRAGMENT_SHADER)?;

        let program = link_program(vertex_shader.get_id(), fragment_shader.get_id())?;

        let mut vertex_array_obj = 0;
        let mut vertex_buf_obj = 0;
        let mut instance_vbo = 0;

        let mut heights: [(f32, f32, f32); 256] = [(-0.5, -0.5, 0.01); 256];

        #[allow(unused_assignments)]
        let mut resolution_location = 0;
        let u_resolution_name = CString::new("u_resolution").map_err(|e| {
            OutputError::new(format!(
                "Failed to create CString from \"u_resolution\": {e}"
            ))
        })?;
        // let mut time_location = 0; // TODO: Somehow use time for something
        unsafe {
            // Use shader program
            gl::UseProgram(program);

            resolution_location = gl::GetUniformLocation(program, u_resolution_name.as_ptr());
            if resolution_location == -1 {
                return Err(OutputError::new(
                    "Failed to get uniform location for u_resolution".to_string(),
                ));
            }
            debug!("Uniform resolution_location: {resolution_location}");

            // time_location =
            //     gl::GetUniformLocation(program, CString::new("u_time").unwrap().as_ptr());
            // if time_location == -1 {
            //     return Err(OutputError::new(
            //         "Failed to get uniform location for u_time".to_string(),
            //     ));
            // }
            // debug!("Uniform time_location: {time_location}");

            gl::GenBuffers(1, &mut instance_vbo);
            gl::BindBuffer(gl::ARRAY_BUFFER, instance_vbo);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                // (heights.len() * std::mem::size_of::<(f32, f32)>()) as isize,
                256 * std::mem::size_of::<(f32, f32, f32)>() as isize,
                heights.as_ptr() as *const c_void,
                // gl::STATIC_DRAW
                gl::DYNAMIC_DRAW,
            );
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);

            gl::GenVertexArrays(1, &mut vertex_array_obj);
            // Create a Vertex Buffer Object and copy the vertex data to it
            gl::GenBuffers(1, &mut vertex_buf_obj);
            gl::BindVertexArray(vertex_array_obj);
            gl::BindBuffer(gl::ARRAY_BUFFER, vertex_buf_obj);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (vertices.len() * std::mem::size_of::<GLfloat>()) as GLsizeiptr,
                #[allow(clippy::useless_transmute)]
                std::mem::transmute(&vertices[0]),
                gl::STATIC_DRAW,
            );

            gl::EnableVertexAttribArray(0);
            gl::VertexAttribPointer(
                0,
                2,
                gl::FLOAT,
                gl::FALSE,
                2 * std::mem::size_of::<f32>() as i32,
                std::ptr::null::<c_void>(),
            );

            // also set instance data
            gl::EnableVertexAttribArray(1);
            gl::BindBuffer(gl::ARRAY_BUFFER, instance_vbo);
            gl::VertexAttribPointer(
                1,
                3,
                gl::FLOAT,
                0,
                3 * std::mem::size_of::<f32>() as i32,
                std::ptr::null::<c_void>(),
            );
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
            gl::VertexAttribDivisor(1, 1);
        }

        let mut i_resolution = [self.width as f32, self.height as f32];
        // let mut time = 0.0;
        let mut event_pump = self.sdl_context.event_pump().unwrap(); // TODO: Error
        'running: loop {
            match self.data_receiver.recv_timeout(Duration::from_millis(100)) {
                Ok(d) => match d {
                    InputData::Heights(h) => {
                        let heights_len = self.calculate_heights(&h) as usize;
                        let spacing = self.spacing as f32 / self.width as f32;
                        let mut x_offset = spacing;
                        let width = (1.0 - spacing * heights_len as f32) / (heights_len as f32);
                        for (i, last_height) in
                            self.last_heights.iter().enumerate().take(heights_len)
                        {
                            heights[i] = (x_offset, *last_height, width);
                            x_offset += width * 2.0 + spacing * 2.0;
                        }

                        unsafe {
                            gl::Uniform2f(resolution_location, i_resolution[0], i_resolution[1]);
                            // gl::Uniform1f(time_location, time);

                            gl::BindBuffer(gl::ARRAY_BUFFER, instance_vbo);
                            gl::BufferSubData(
                                gl::ARRAY_BUFFER,
                                0,
                                256 * std::mem::size_of::<(f32, f32, f32)>() as isize,
                                heights.as_ptr() as *const c_void,
                            );
                            gl::BindBuffer(gl::ARRAY_BUFFER, 0);

                            gl::ClearColor(self.bg_color.0, self.bg_color.1, self.bg_color.2, 1.0);
                            gl::Clear(gl::COLOR_BUFFER_BIT);

                            gl::UseProgram(program);
                            gl::BindVertexArray(vertex_array_obj);
                            gl::DrawArraysInstanced(
                                gl::TRIANGLES,
                                0,
                                6,
                                heights_len as i32, // heights.len() as i32
                            );
                            gl::BindVertexArray(0);
                        }
                    }
                    InputData::Quit => {
                        debug!("Received quit message");
                        break 'running;
                    }
                },
                Err(e) => {
                    if e == RecvTimeoutError::Disconnected {
                        warn!("Input channel disconnected");
                        break 'running;
                    }
                }
            }

            self.window.gl_swap_window();

            // time += 0.1;

            for event in event_pump.poll_iter() {
                match event {
                    Event::Quit { .. }
                    | Event::KeyDown {
                        keycode: Some(Keycode::Escape),
                        ..
                    } => break 'running,
                    Event::Window {
                        win_event: WindowEvent::Resized(w, h),
                        ..
                    } => {
                        self.width = w;
                        self.height = h;
                        unsafe {
                            gl::Viewport(0, 0, self.width, self.height);
                        }
                        i_resolution[0] = w as f32;
                        i_resolution[1] = h as f32;
                    }
                    Event::KeyDown {
                        keycode: Some(Keycode::Plus),
                        ..
                    } => {
                        if self.point_count <= 128 {
                            // Max limit of 256
                            self.point_count *= 2;
                        }
                    }
                    Event::KeyDown {
                        keycode: Some(Keycode::Minus),
                        ..
                    } => {
                        if self.point_count >= 4 {
                            // Min limit of 2
                            self.point_count /= 2;
                        }
                    }
                    _ => {}
                }
            }
        }

        self.input_quit
            .store(true, std::sync::atomic::Ordering::SeqCst);

        unsafe {
            gl::DeleteProgram(program);
            gl::DeleteShader(fragment_shader.get_id());
            gl::DeleteShader(vertex_shader.get_id());
            gl::DeleteBuffers(1, &vertex_buf_obj);
            gl::DeleteVertexArrays(1, &vertex_array_obj);
        }

        Ok(())
    }
}

impl GUIOpenGL {
    pub fn set_bg_color(&mut self, color: (f32, f32, f32)) {
        self.bg_color = color;
    }

    pub fn set_spacing(&mut self, spacing: u32) {
        self.spacing = spacing;
    }

    pub fn set_shaders_path(&mut self, vert: PathBuf, frag: PathBuf) {
        self.vert_shader_path = Some(vert);
        self.frag_shader_path = Some(frag);
    }

    #[inline(always)]
    fn calculate_heights(&mut self, h: &Vec<f32>) -> u32 {
        let mut heights: Vec<f32> = Vec::new();
        let mut update_heights = false;
        // Update the cached logspace for frequency indexes
        if h.len() != self.indexes.1 || self.point_count != self.indexes.2 {
            debug!("Recaching index logspace");

            if self.point_count != self.indexes.2 {
                self.indexes.2 = self.point_count;
                update_heights = true;
            }
            let m = h.len().ilog2() as f32;
            self.indexes.0 = Array::logspace(2.0, 0.0, m, self.point_count)
                .map(|n| (*n).ceil() as usize)
                .to_vec();
            self.indexes.1 = h.len();
        }

        let mut c = 0;
        let mut point_count = 0;
        for _i in 0..self.indexes.0.len() {
            let i = self.indexes.0[_i];
            if _i > 1 {
                // Skip duplicating bars for a nicer visualizing
                // This will reduce the total amount of points
                if self.indexes.0[_i - 1] == self.indexes.0[_i] {
                    continue;
                }
            }

            let mut sum: f32 = 0.0;
            let x = i - c;
            let mut max = 0.0;
            // Calculate average of heights between last and current index
            while c < i {
                sum += h[c];
                if h[c] > max {
                    max = h[c];
                }
                c += 1;
            }

            // Store the average height for the interval
            sum /= x as f32;
            heights.push(
                (if x < 1 {
                    h[c]
                } else {
                    // Add the difference between the average and the max height in the interval to
                    // the average to inflate the heights for a smoother look in terms of bar heights
                    sum + (max - sum)
                })
                .clamp(0.0, 1.0),
            );

            point_count += 1;
        }

        if update_heights
            || (self.last_heights_len != h.len() && self.last_heights.len() != heights.len())
        {
            self.last_heights = heights;
            self.last_heights_len = h.len();
        } else {
            let mut i = 0;
            while i < self.last_heights.len() {
                // To not bug the shit we map NaN values to 0.0
                if self.last_heights[i].is_nan() {
                    self.last_heights[i] = 0.0;
                }
                if heights[i].is_nan() {
                    heights[i] = 0.0;
                }

                // Smoothing
                if self.last_heights[i] < heights[i] {
                    self.last_heights[i] +=
                        (heights[i] - self.last_heights[i]) / self.smoothing_divisor;
                } else if self.last_heights[i] > heights[i] {
                    self.last_heights[i] -=
                        (self.last_heights[i] - heights[i]) / (self.smoothing_divisor * 2.0);
                }

                i += 1;
            }
        }

        point_count
    }
}
