/*
 * This file is part of bolger
 *
 * Copyright (C) 2023  marcu5h <marlhan@proton.me>
 *
 * bolger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bolger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bolger.  If not, see <https://www.gnu.org/licenses/>.
 */

pub mod opengl;
pub mod output_error;
pub mod sdl;

use std::sync::{atomic::AtomicBool, mpsc::Receiver};

use sdl2::Sdl;

use crate::input::InputData;

use self::output_error::OutputError;

pub trait Output<'a> {
    fn new(
        title: &'a str,
        data_receiver: Receiver<InputData>,
        input_quit: &'static AtomicBool,
        sdl_context: Option<Sdl>,
    ) -> Result<Self, OutputError>
    where
        Self: Sized;

    fn run(&mut self) -> Result<(), OutputError>;
}
