/*
 * this file is part of bolger
 *
 * copyright (c) 2023  marcu5h <marlhan@proton.me>
 *
 * bolger is free software: you can redistribute it and/or modify
 * it under the terms of the gnu general public license as published by
 * the free software foundation, either version 3 of the license, or
 * (at your option) any later version.
 *
 * bolger is distributed in the hope that it will be useful,
 * but without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.  see the
 * gnu general public license for more details.
 *
 * you should have received a copy of the gnu general public license
 * along with bolger.  if not, see <https://www.gnu.org/licenses/>.
 */

use std::{
    error::Error,
    fmt::{self, Display, Formatter},
    fs::File,
    io::Read,
    path::PathBuf,
    str::FromStr,
};

use log::debug;
use sdl2::pixels::Color;
use serde::Deserialize;

use crate::output::sdl::{Bars, Circles, Lines, VisualMode};

#[derive(Debug)]
pub struct ConfigError {
    msg: String,
}

impl Error for ConfigError {}

impl ConfigError {
    pub fn new(msg: String) -> Self {
        Self { msg }
    }
}

impl Display for ConfigError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.msg)
    }
}

#[derive(Deserialize, Debug)]
struct TomlConfig {
    pub output: Option<String>,
    pub sdl: Option<TomlSdl>,
    pub opengl: Option<TomlOpenGL>,
}

#[derive(Deserialize, Debug)]
struct TomlSdl {
    pub bg_image: Option<String>,
    pub bg_image_color_mod: Option<String>,
    pub mode: Option<String>,
    pub mode_options: Option<TomlModeOpts>,
}

#[derive(Deserialize, Debug)]
struct TomlOpenGL {
    pub bg_color: Option<String>,
    pub spacing: Option<u32>,
    pub vert_shader: Option<String>,
    pub frag_shader: Option<String>,
}

#[derive(Deserialize, Clone, Debug)]
struct TomlModeOpts {
    pub spacing: Option<i32>,
    pub color: Option<String>,
    pub show_point: Option<bool>,
    pub point_color: Option<String>,
    pub point_radius: Option<i16>,
}

impl Default for TomlModeOpts {
    fn default() -> Self {
        Self {
            spacing: Some(2),
            color: Some("#FFFFFF".to_string()),
            show_point: Some(false),
            point_color: Some("#FFFFFF".to_string()),
            point_radius: Some(5),
        }
    }
}

pub enum Input {
    File(PathBuf),
    System,
}

pub enum OutputEnum {
    SDL,
    OpenGL,
}

pub struct Config {
    pub input: Input,
    pub output: OutputEnum,
    pub sdl: Sdl,
    pub opengl: OpenGL,
}

pub struct Sdl {
    pub bg_image: Option<PathBuf>,
    pub bg_image_color_mod: Option<(u8, u8, u8)>,
    pub mode: VisualMode,
}

pub struct OpenGL {
    pub bg_color: (f32, f32, f32),
    pub spacing: u32,
    pub vert_shader: PathBuf,
    pub frag_shader: PathBuf,
}

fn hex_word_to_int(w: &[u8; 2]) -> Result<u8, ConfigError> {
    let mut val = 0;
    match w[0] as char {
        // TODO: Avoid cast
        '0' => {}
        '1' => val += 16,
        '2' => val += 32,
        '3' => val += 48,
        '4' => val += 64,
        '5' => val += 80,
        '6' => val += 96,
        '7' => val += 112,
        '8' => val += 128,
        '9' => val += 144,
        'a' => val += 160,
        'b' => val += 176,
        'c' => val += 192,
        'd' => val += 208,
        'e' => val += 224,
        'f' => val += 240,
        _ => {
            return Err(ConfigError::new(format!("Invalid hex value: {}", w[1])));
        }
    }
    match w[1] as char {
        // TODO: Avoid cast
        '0' => {}
        '1' => val += 1,
        '2' => val += 2,
        '3' => val += 3,
        '4' => val += 4,
        '5' => val += 5,
        '6' => val += 6,
        '7' => val += 7,
        '8' => val += 8,
        '9' => val += 9,
        'a' => val += 10,
        'b' => val += 11,
        'c' => val += 12,
        'd' => val += 13,
        'e' => val += 14,
        'f' => val += 15,
        _ => {
            return Err(ConfigError::new(format!("Invalid hex value: {}", w[1])));
        }
    }

    Ok(val)
}

fn parse_hex_str(s: &str) -> Result<(u8, u8, u8), ConfigError> {
    let chars = s.to_lowercase();
    let chars = chars.as_bytes();
    Ok((
        hex_word_to_int(&[chars[1], chars[2]])?,
        hex_word_to_int(&[chars[3], chars[4]])?,
        hex_word_to_int(&[chars[5], chars[6]])?,
    ))
}

fn str_to_color(s: &str) -> Result<Color, ConfigError> {
    if s.len() != "#000000".len() || !s.starts_with('#') {
        return Err(ConfigError::new(format!("Invalid color format: {s}")));
    }

    let rgb = parse_hex_str(s)?;
    Ok(Color::RGB(rgb.0, rgb.1, rgb.2))
}

// TODO: Test
fn str_to_color_float(s: &str) -> Result<(f32, f32, f32), ConfigError> {
    if s.len() != "#000000".len() || !s.starts_with('#') {
        return Err(ConfigError::new(format!("Invalid color format: {s}")));
    }

    let rgb = parse_hex_str(s)?;
    Ok((
        rgb.0 as f32 / 255.0,
        rgb.1 as f32 / 255.0,
        rgb.2 as f32 / 255.0,
    ))
}

fn str_to_opengl_color(s: &Option<String>) -> Result<(f32, f32, f32), ConfigError> {
    let Some(s) = s else {
        return Err(ConfigError::new("No bg_color provided".to_string()));
    };

    str_to_color_float(s)
}

fn str_to_color_mod(s: Option<String>) -> Result<Option<(u8, u8, u8)>, ConfigError> {
    let Some(s) = s else {
        return Ok(None);
    };

    if s.len() != "#000000".len() || !s.starts_with('#') {
        return Err(ConfigError::new(format!("Invalid color format: {s}")));
    }

    Ok(Some(parse_hex_str(&s)?))
}

fn mode_from_toml(sdl_toml: &TomlSdl) -> Result<VisualMode, ConfigError> {
    let Some(mode) = &sdl_toml.mode else {
        return Err(ConfigError::new("SDL mode is none".to_string()));
    };
    let opts = match sdl_toml.mode_options.clone() {
        // TODO: workaround clone
        Some(o) => o,
        None => {
            debug!("No SDL mode options given");
            TomlModeOpts::default()
        }
    };
    match mode.to_lowercase().as_str() {
        "bars" => Ok(VisualMode::Bars(Bars {
            spacing: opts.spacing.unwrap_or(2),
            bar_color: str_to_color(&opts.color.unwrap_or("#FFFFFF".to_string()))?,
        })),
        "circles" => Ok(VisualMode::Circles(Circles {
            spacing: opts.spacing.unwrap_or(2),
            circle_color: str_to_color(&opts.color.unwrap_or("#FFFFFF".to_string()))?,
        })),
        "lines" => Ok(VisualMode::Lines(Lines {
            line_color: str_to_color(&opts.color.unwrap_or("#FFFFFF".to_string()))?,
            show_point: opts.show_point.unwrap_or(false),
            point_color: str_to_color(&opts.point_color.unwrap_or("#FFFFFF".to_string()))?,
            point_radius: opts.point_radius.unwrap_or(5),
        })),
        _ => Err(ConfigError::new(format!("Invalid visual mode: {mode}"))),
    }
}

fn path_buf_from_option(path: &Option<String>, what: &str) -> Result<PathBuf, ConfigError> {
    let Some(path) = path else {
        return Err(ConfigError::new(format!("{what}: no path provided")));
    };

    Ok(PathBuf::from(path))
}

fn toml_to_config(toml: &TomlConfig) -> Result<Config, ConfigError> {
    let output = match &toml.output {
        Some(out) => match out.to_lowercase().as_str() {
            "sdl" => OutputEnum::SDL,
            "opengl" => OutputEnum::OpenGL,
            _ => {
                return Err(ConfigError::new(format!("Invalid output provided: {out}")));
            }
        },
        None => {
            log::info!("No output provided. Using SDL");
            OutputEnum::SDL
        }
    };

    Ok(Config {
        input: 'input: {
            let args: Vec<String> = std::env::args().collect();
            for arg in args.iter().skip(1) {
                match arg.as_str() {
                    "-h" | "--help" => {
                        todo!();
                    }
                    _ => {
                        debug!("Unknown argument provided ({arg}). Threating as input file");
                        break 'input Input::File(PathBuf::from(arg));
                    }
                }
            }

            Input::System
        },
        output,
        opengl: {
            match &toml.opengl {
                Some(_opengl) => OpenGL {
                    bg_color: str_to_opengl_color(&_opengl.bg_color)?,
                    spacing: _opengl.spacing.unwrap_or(0),
                    vert_shader: path_buf_from_option(
                        &_opengl.vert_shader,
                        "OpenGL vertex shader",
                    )?,
                    frag_shader: path_buf_from_option(
                        &_opengl.frag_shader,
                        "OpenGL fragment shader",
                    )?,
                },
                None => {
                    return Err(ConfigError::new("Missing OpenGL config".to_string()));
                }
            }
        },
        sdl: {
            match &toml.sdl {
                Some(_sdl) => Sdl {
                    bg_image: _sdl.bg_image.as_ref().map(PathBuf::from),
                    bg_image_color_mod: str_to_color_mod(_sdl.bg_image_color_mod.clone())?,
                    mode: mode_from_toml(_sdl)?,
                },
                None => {
                    return Err(ConfigError::new("Missing SDL config".to_string()));
                }
            }
        },
    })
}

pub fn parse_config() -> Result<Config, ConfigError> {
    let config_dir = 'config_dir: {
        match std::env::var("XDG_CONFIG_HOME") {
            Ok(p) => break 'config_dir p,
            Err(e) => debug!("Failed to get $XDG_CONFIG_HOME: {e}"),
        }

        match std::env::var("HOME") {
            Ok(p) => format!("{p}/.config"),
            Err(e) => {
                return Err(ConfigError::new(format!("Failed to get $HOME: {e}")));
            }
        }
    };

    let mut config_file_path =
        PathBuf::from_str(&config_dir).map_err(|e| ConfigError::new(e.to_string()))?;
    config_file_path.push("bolger/bolger.toml");
    if !config_file_path.exists() {
        return Err(ConfigError::new(format!(
            "Config file ({config_file_path:?}) does not exist"
        )));
    }

    let mut config_file = File::open(&config_file_path)
        .map_err(|e| ConfigError::new(format!("Failed to open {config_file_path:?}: {e}")))?;

    let mut toml_string = String::new();
    config_file
        .read_to_string(&mut toml_string)
        .map_err(|e| ConfigError::new(format!("Failed to read from {config_file_path:?}: {e}")))?;

    let toml_config: TomlConfig = toml::from_str(&toml_string)
        .map_err(|e| ConfigError::new(format!("Failed to deserialize toml: {e}")))?;

    debug!("{toml_config:?}");

    toml_to_config(&toml_config)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn invalid_color() {
        assert_eq!(str_to_color("#FAKEEE").is_err(), true);
    }

    #[test]
    fn invalid_color2() {
        assert_eq!(str_to_color("Not valid").is_err(), true);
    }

    #[test]
    fn valid_color() {
        assert_eq!(str_to_color("#112233").is_ok(), true);
    }

    #[test]
    fn valid_color2() {
        assert_eq!(str_to_color("#ABCDEF").is_ok(), true);
    }

    #[test]
    fn hex_word_to_int_test() {
        assert_eq!(
            hex_word_to_int(&['f' as u8, 'f' as u8]).expect("Failed to convert hex word to int"),
            0xFF
        );
    }

    #[test]
    fn hex_word_to_int_test2() {
        assert_eq!(
            hex_word_to_int(&['a' as u8, '1' as u8]).expect("Failed to convert hex word to int"),
            0xA1
        );
    }
}
