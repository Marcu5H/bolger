# nix-build -E "with import <nixpkgs> {}; callPackage ./default.nix {}"
{ lib 
, fetchFromGitLab
, fetchFromGitHub
, callPackage
, pkg-config
, makeRustPlatform
, SDL2
, SDL2_gfx
, SDL2_image
, pipewire
, libclang
, llvmPackages
, gst_all_1
, glibc
}:
let
  mozillaOverlay = fetchFromGitHub {
    owner = "mozilla";
    repo = "nixpkgs-mozilla";
    rev = "6eabade97bc28d707a8b9d82ad13ef143836736e";
    hash = "sha256-1ElPLD8eFfnuIk0G52HGGpRtQZ4QPCjChRlEOfkZ5ro=";
  };
  mozilla = callPackage "${mozillaOverlay.out}/package-set.nix" {};
  rustNightly = (mozilla.rustChannelOf { date = "2023-11-27"; channel = "nightly"; }).rust;
in (makeRustPlatform {
  cargo = rustNightly;
  rustc = rustNightly;
}).buildRustPackage rec {
  pname = "bolger-${version}";
  version = "0.1.2";

  src = fetchFromGitLab {
    owner = "marcu5h";
    repo = "bolger";
    rev = "v0.1.2";
    hash = "sha256-RRcUiYAKoalt7CGTM+Mv7RqGUZuQ8UCwGrshBMr/CuE=";
  };

  cargoHash = "sha256-HKcXj9/8WaF8wtcEPp+9Wp2MXkg17jjY+hTxDxH5dJ8=";

  nativeBuildInputs = [
    pkg-config
  ];

  buildInputs = [
    SDL2
    SDL2_gfx
    SDL2_image
    pipewire
    libclang
    gst_all_1.gstreamer
    gst_all_1.gst-plugins-base
    gst_all_1.gst-plugins-good
  ];

  LD_LIBRARY_PATH = lib.makeLibraryPath buildInputs;
  BINDGEN_EXTRA_CLANG_ARGS = [
    ''-I"${llvmPackages.libclang.lib}/lib/clang/${llvmPackages.libclang.version}/include"''
    "-I ${glibc.dev}/include"
  ];

  meta = with lib; {
    description = "Audio visualizer";
    homepage = "https://gitlab.com/Marcu5H/bolger";
    license = licenses.gpl3;
    maintainers = [];
  };
}
