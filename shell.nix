{ pkgs ? import <nixpkgs> {} }:

with pkgs;

mkShell rec {
  nativeBuildInputs = [
    pkg-config
    clang
  ];
  buildInputs = [
    SDL2
    SDL2_gfx
    SDL2_image
    pipewire
    libclang
    gst_all_1.gstreamer
    gst_all_1.gst-plugins-base
    gst_all_1.gst-plugins-good
  ];

  LD_LIBRARY_PATH = lib.makeLibraryPath buildInputs;
  BINDGEN_EXTRA_CLANG_ARGS = [
    ''-I"${pkgs.llvmPackages.libclang.lib}/lib/clang/${pkgs.llvmPackages.libclang.version}/include"''
    "-I ${pkgs.glibc.dev}/include"
  ];
  shellHook = ''
    # export GST_DEBUG=3
    '';
}
