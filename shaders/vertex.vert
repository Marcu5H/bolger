#version 330 core

layout (location = 0) in vec2 position;
layout (location = 1) in vec3 offset;

void main() {
  float x = position.x;
  float y = position.y;
  if (y == -1) {
    if (x == -0.1) {
        gl_Position = vec4(offset.x + (offset.z * 2 - 1.0), y, 0.0, 1.0);
    } else {
        gl_Position = vec4(x + offset.x, y, 0.0, 1.0);
    }
  } else {
    if (x == -0.1) {
        gl_Position = vec4(offset.x + (offset.z * 2 - 1.0), y * (offset.y * 2 - 1.0), 0.0, 1.0);
    } else {
        gl_Position = vec4(x + offset.x, y * (offset.y * 2 - 1.0), 0.0, 1.0);
    }
  }
}
