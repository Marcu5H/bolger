#version 330 core

uniform vec2 u_resolution;
uniform float u_time;

out vec4 out_color;

void main(void) {
  float rg = gl_FragCoord.y / u_resolution.y;

  vec3 green = vec3(0.0, 1.0, 0.0);
  vec3 yellow = vec3(1.0, 1.0, 0.0);
  vec3 red = vec3(1.0, 0.0, 0.0);

  vec3 color = mix(green, yellow, smoothstep(0.35, 0.65, rg));
  color = mix(color, red, smoothstep(0.85, 0.95, rg));

  out_color = vec4(color, 1.0);
}
